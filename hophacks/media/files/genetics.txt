CENTRAL DOGMA
CENTRAL DOGMA
CENTRAL DOGMA
CENTRAL DOGMA
CENTRAL DOGMA
CENTRAL DOGMA
CENTRAL DOGMA
CENTRAL DOGMA

Transcription to translation.


Antiparallel, double-stranded DNA structure: 
	- phosphates and sugar are hydrophillic
	- nitrogen bases ATGC are hydrophobic
	- surrounded by water and shit

Forces that stabilize DNA structure:
	- hydrogen bonding in the nitrogen bases
	- covalent bonds otherwise
	- base stacking interactions are due to dispersion attracton.
		- important in secondary structure, tertiary structure, stem-loop structures are stabilized by base stacking
		- london dispersion forces and partial charges

Taxonomy vs Phylogeny:
	- Taxonomy is more of the physical characteristics of organisms DKPCOFGS
	- Phylogeny focuses more on evolutionary relationships
		- the underlying principle of phylogeny is evolution and that all living things are related.

Identification and classification of organisms:
	- Nucleus: eukaryotes have them, bacteria and archaea don't.
	- Organelles: eukaryotes have membrane bound organelles, but the other two don't.
	- Peptidoglycan: only present for cell walls in bacteria.
	- Ester linkage: present in bacteria and eukaryotes in unbranched fatty chains.
	- Ether linkage: present only in archaea of hydrocarbon attached to glycerol.
	- Ribosomes: eukarya have 80s ribosomes while bacteria and archaea have 70s. (prokaryotes)

Gram Stain:
	- Cell walls of gram+ organisms have a higher peptidoglycan and lower lipid count.
	- We first stain the bacteria and the crystal violet is taken in by the cell.
	- Then we add iodine, and the complex it forms with the crystal violet makes it harder to be remove.
	- We then dissolve the lipid layer of the gram negative cells with ethanol.
	- In gram positive cells, this dehydrates and closes the pores in the cell wall, thus making the violet-iodine complex stay inside.
	- Then we counterstain to identify the gram negative bacteria.

Polymerase Chain Reaction:
	- First we denature the DNA by heating. This separates the DNA into single strands. Hydrogen bonds break at high temperatures.
	- We then use custom primers to bind to specific places in DNA. These primers are usually around 20-30 bases and are very specific.
	- We then carry out extension, and we use Taq DNA polymerase to replicate the DNA strands. It begins at the region marked by the primers.
	- Don't forget about the dNTPS.



16S rRNA/ bacterial microbiome:
	- Symbiotic relationships within bacterial microbiomes.
	- A component of the 30S small subunit of prokaryotic ribosomes.
	- There are highly conserved primer binding sites which make PCR very easy.
	- There are also hypervariable regions that provide species specific sequences.
	- Good in hard to culture bacteria because we need very little DNA to perform PCR.

Construction of a plasmid library:
	- A plasmid is a piece of circular DNA which are transformed into bacteria.
	- The plasmid libary is the total genomic DNA in a single organism.  These are inserted into the vector using the enzyme, DNA ligase.
	- The vector DNA can then be taken up by a host organism.
	LOOK OVER THIS SECTION AGAIN
	LOOK OVER THIS SECTION AGAIN
	LOOK OVER THIS SECTION AGAIN
	LOOK OVER THIS SECTION AGAIN
	LOOK OVER THIS SECTION AGAIN


External Controls on Gene Expression: Increase or decrease the production of specific gene prodcts.
	- Transcriptional activator proteins (cyclic AMP): Increases gene transcription of a gene. These are usually proteins that bind to specific sites of DNA located in or near a promoter. cAMP activates transcription at the lac operon in E. coli. During glucose starvation, cAMP binds to CAP which recruits RNA polymerase to the promoter.
	- Transcriptional repressor proteins (Lac operon repressor): A protein that binds to the operator, thereby blocking attachment of RNA polymerase to the promoter and therefore, halting transcription. In the lac operon, lacZYA transcribes the proteiins needed for lactose breakdown. allolactose the repressor changes conformation and falls off the operator.
	- Prokaryotic toxin/antitoxin system: Causes cell cycle arrest when responding to stresses which causes the cells to die.
	- Eukaryotic microRNA pathway: RISC Dicer LOOK INTO TOMORROW.

Non-coding RNA Control Mechanisms:
	- http://www.nature.com/scitable/topicpage/small-non-coding-rna-and-gene-expression-1078





Sense = template = top
antisense = coding = mrnalike = bottom

-10 tata

Transcription/Translation (The Central Dogma)
-35 -10 +1
Review a structure of the DNA.







Mutagen: a chemical or physical agent that causes mutations
Base analogs
Deaminating agents removal of amino group
Alkylating agents add alkyl groups to DNA. Prevent progess of the replication complex.

Direct Repair: act directly on damaged nucleotides.
	- Nicks can be repaired by DNA ligase by replacing phosphodiester bonds.
	- Alkylation can be actively transported out from the nucleotide to the polypeptide chain of an enzyme. Ada enzyme.
	- Cyclobutyl dimers through photoreactivation. DNA photolayse.
Excision repair: excision of the polynucleotide containing a damaged site and then resynthesizes using DNA Polymerase.
	- Two types: base and nucleotide.
	- Base, relatively non-complex. Removal of one or more damaged nucleotides followed by resynthesis.
	- Initiated by DNA glycoslyase which has limited specificity
	- Nucleotide excision is more broad and complex, but lets us correct more extensive types of damage and longer sequences.
	- Short path/long path in E. coli.

Mismatch repair: unlike direct and excision repair, mismatch repair finds DNA damage where bases are not paired up correctly. This is done by searching for absence of base-pairing. In E. Coli, we look for the methylation through Dam methylase and Dcm cytosine methylase.


More in depth about template vs newly synthesized:
The daughter strand is unmethylated and can be distinguished from the p[arent strand since the parent strand has a full complement of methyl groups. E. coli DNA is methylated due to dam methylase ande dcm methylase/

Uses MutH, MutL, and MutS for long patch repair in mismatch. MutL is a bit unclear on its role. MutH binds to the 5-3 GATTC seequence while MutS recognizes the mismatch sites. MutH cuts the phosphodiester and DNA helicase II detatches the single strand. This is degraded by exonuclease that follows the helicase. DNA polymerase and DNA ligase then fill in the hole.

SOS Response, mutasome that takes the place of DNA Polymerase to carry out synthesis of damaged region.

Recombination: Holliday model describes homologous recombination, the most important version of recombination. Meiotic crossing-over. Formation of a heteroduplex resulting from the exchange of polynucleatide segments between the two homologous molecules.


Okazaki Fragments: Relatively short polymers of DNA that are synthesized in association with the lagging strand during DNA replication.

Replication slippage: DNA Polymerase encounteres a direct repeat during the replication process. The polymerase complex suspends replication, and the newly synthesized strand detatches from the template strand. DNA polymerase resumes normal replication but it backtracks a bit and repeats the insertion of dntps that were added. This leads to a repeat being repeated twice.
Huntingdon's disease. HD locus a lot more repeats. 

Exonuclease activity: 3'5 DNA stops, exonuclease catches up, works back 3->5.

Requirements of DNA Replication:

Natural Selection: The process by which evolution occurs.
Allopatric: Barrier form dividing into two.
Peripatric: New niche is entered.
Parapatric: A peripheral niche.
Sympatric: Within the ppulation.

Gradualism, speciation, common ancestry: Gradual, slow changes in evolutions. Continuous.
Evidence for common ancestry: Same coding for proteins. We all use DNA/RNA. Ribosomes highly conserved.
ATP is used as energy by all organisms.












Horizontal transfer of DNA: Transfer of genes between organisms in a manner other than reproduction.
Natural transformation: Transformation requires the bacterium to be competent, that is, enter a special physiological state. Generally happens much momre in prokaryotic cells. Takes up plasmids and vectors.

Transduction by bacteriophages: (generalized, abortive, and specialized): When DNA is transferred from one bacterium to another by a virus. Generalized is when it instantly lyses the cell and takes over the functions. Specialized is when the phage is more temperate and waits for the command to lyse.
Abortive is when the DNA is not incorporated into the bacterial genome, so it can only be transmitted not propagated.


Conjugation: A bacterial equivalent to sexual reproduction. This involves direct contact between cells  Direct exchange of genetic material. Uses a protein tube called F pils.

Why bacterial taxonomy is hard: ue to biological characteristics (e.g. frequent horizontal transfer of DNA) and to historical typing methods (e.g. pathotypes of E. coli were defined by diseases they caused, rather than genes they carry).


Sanger (dideoxy) Sequencing and Sanger chromatogram: 4 separate reactions. ddNTPs and dNTPs primers and DNA polymerase. Makes some with ddNTPs which terminate transcription. Run gel electrophoresis on them.

Next-generation Sequencing with Illumina platform: Clusters are made on the flat surface. DNA attached to the gel. Flourescent nucleotides and excitation by lasers.

Database searching using BLASTN: A very widely used bioinformatics program. It searchs to complete a query sequence with a library or database of sequences. It then identifies library sequences that resemble the query sequence above a certain threshold.

(in 16S rRNA)
Alpha Diversity: Differences in the actual organisms.
Beta Diversity: Differences in the habitat.
