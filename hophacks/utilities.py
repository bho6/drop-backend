from facebook import GraphAPI


def get_user_profile(access_token):
    """Return user profile in (user_id, user_name)
    or throws ValueError for invalid token.
    """
    graph = GraphAPI(access_token)
    response = graph.get_object("me")
    if response.get("error"):
        return ValueError
    return int(response["id"]), response["name"]


TEST_TOKEN = ("CAAGNkyrqJgoBAJ0RSU0I0cCZAaln69S6Dst6fTFir7i2nFvpKsHq"
              "nr4S04hgy3qHZBIKIvMnZBkEJJXWrfESweokbLXmGN4sgL98ZBp8V"
              "5UfB9UkbS7YjhgZBRsBVFumOPs7IlDOxGnZBt36XkCTcLABCIq3t2"
              "N82ZC8QRMJahnmwzbngAc3v833wcy0vgBdOgZD")


def test():
    assert get_user_profile(TEST_TOKEN) == (1529297245, u"Mozhi Zhang")


if __name__ == "__main__":
    test()
