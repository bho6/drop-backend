from django.conf.urls import patterns, include, url
from user.views import LoginView
from entry.views import EntryView
from drop.views import DropView
from testpage.views import TestView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^login/$', LoginView.as_view(), name = 'login'),
    url(r'^entry/$', EntryView.as_view(), name = 'entry'),
    url(r'^drop/$', DropView.as_view(), name = 'drop'),
    url(r'^testpage/$', TestView.as_view(), name = 'testpage'),
    url(r'^admin/', include(admin.site.urls)),
)