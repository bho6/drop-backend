from django.db import models


class FacebookUser(models.Model):
    fbuser_id = models.IntegerField(primary_key=True)
    fbuser_access_token = models.TextField()
    fbuser_name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.fbuser_name

    @classmethod
    def login_user(cls, name, uid, token):
        fb = FacebookUser.objects.filter(fbuser_id=uid)
        if fb.exists():
            fb[0].fbuser_access_token = token
            fb[0].fbuser_name = name
        else:
            FacebookUser(fbuser_id=uid,
                         fbuser_access_token=token,
                         fbuser_name=name).save()

    @classmethod
    def get_uid_by_token(cls, token):
        try:
            fb = FacebookUser.objects.get(fbuser_access_token=token)
        except FacebookUser.DoesNotExist:
            return None
        return fb.fbuser_id
