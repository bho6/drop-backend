from django.views.generic import View
from django.http import HttpResponse
from user.models import FacebookUser
from hophacks.utilities import get_user_profile
from facebook import GraphAPIError
import json

class LoginView(View):
    def post(self, request, *args, **kwargs):
    	if 'access_token' in request.POST:
    		try:
    			token = request.POST['access_token']
    			user_id, user_name = get_user_profile(token)
    			FacebookUser.login_user(user_name, user_id, token)
    			response_data = {'user_id':  user_id, 'user_name': user_name}
    			return HttpResponse(json.dumps(response_data), mimetype="application/json")
    		except (ValueError, GraphAPIError):
    			return HttpResponse(json.dumps({'error': 'Invalid parameter.'}), mimetype="application/json")