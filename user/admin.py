from django.contrib import admin
from user.models import FacebookUser
from drop.models import Drop
from entry.models import Entry

admin.site.register(FacebookUser)
admin.site.register(Drop)
admin.site.register(Entry)
