from django.views.generic import TemplateView
from django.http import HttpResponseRedirect, HttpResponse

class TestView(TemplateView):
    template_name = "template.html"

    # def get_comments_dict(self):
    # 	comments = Comment.objects.order_by('id')
    # 	com_list = []
    # 	for com in comments:
    # 		com_dict = {'date': com.date(),
    # 					'name': com.comment_name,
    # 					'msg': com.comment_body}
    # 		com_list.append(com_dict)
    # 	return com_list

    # def get(self, request, *args, **kwargs):
    #     context = {'com_list': self.get_comments_dict()}
    #     return self.render_to_response(context)

    # def post(self, request, *args, **kwargs):
    # 	if 'name' in request.POST and 'message' in request.POST:
    # 			c = Comment(comment_name = request.POST['name'], comment_body = request.POST['message'])
    # 			c.save()
    # 			return HttpResponseRedirect('/comment/')
    # 	context = {'com_list': self.get_comments_dict()}
    # 	return self.render_to_response(context)