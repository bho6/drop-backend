from django.db import models
from drop.models import Drop
from user.models import FacebookUser

class Entry(models.Model):
	entry_file = models.ForeignKey(Drop)
	entry_user = models.ForeignKey(FacebookUser)
	entry_latitude = models.FloatField()
	entry_longitude = models.FloatField()
	entry_expiration_date = models.DateTimeField()
	entry_expiration_views = models.IntegerField(blank=True, null=True)
	entry_radius = models.IntegerField()
	entry_created = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return str(self.entry_latitude) + ", " + str(self.entry_longitude)
