from django.views.generic import View
from django.http import HttpResponse
from datetime import timedelta
from django.utils.timezone import now
from user.models import FacebookUser
from drop.models import Drop
from entry.models import Entry
import json
import datetime
import math
 
def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km
 
    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c
 
    return d * 1000


class EntryView(View):
    def get(self, request, *args, **kwargs):
        error_response = HttpResponse(json.dumps(
            {"error": "Invalid parameter."}),
            mimetype="application/json")
        try:
            #user_id = request.GET.get("access_token")
            latitude = float(request.GET["latitude"])
            longitude = float(request.GET["longitude"])
            radius = float(request.GET["radius"])
        except KeyError:
            return error_response

        entries = Entry.objects.all()
        # if user_id:
        #     entries = entries.filter(entry_drop.drop_user.id == user_id)

        in_range = lambda e: (distance((e.entry_latitude, e.entry_longitude),
                                      (latitude, longitude)) <= e.entry_radius
                              and e.entry_expiration_date > now()
                              and distance((e.entry_latitude, e.entry_longitude), 
                                           (latitude, longitude)) <= radius)
        entries = filter(in_range, entries)
        result_list = [{"entry_id": e.id, "latitude": e.entry_latitude,
                        "longitude": e.entry_longitude,
                        "file_id": e.entry_file.id,
                        "user_id": e.entry_file.drop_user.fbuser_id}
                       for e in entries]
        return HttpResponse(json.dumps({"response": result_list}),
                            mimetype="application/json")

    def post(self, request, *args, **kwargs):
        error_response = HttpResponse(json.dumps(
            {"error": "Invalid parameter."}),
            mimetype="application/json")
        success_response = HttpResponse(json.dumps(
            {"response": "Success."}), mimetype="application/json")
        try:
            access_token = request.POST["access_token"]
            latitude = float(request.POST["latitude"])
            longitude = float(request.POST["longitude"])
            radius = float(request.POST["radius"])
            file_id = int(request.POST["file_id"])
            expires_in = int(request.POST["expires_in"])
        except KeyError:
            return error_response

        uid = FacebookUser.get_uid_by_token(access_token)
        if not uid:
            return error_response

        try:
            drop = Drop.objects.get(id=file_id)
        except Drop.DoesNotExist:
            return error_response

        new_entry = Entry(entry_file=drop, entry_user=FacebookUser.objects.get(fbuser_id=uid),
                          entry_latitude=latitude,
                          entry_longitude=longitude,
                          entry_expiration_date=now(),
                          entry_expiration_views=-1,
                          entry_radius=radius)
        new_entry.save()
        new_entry.entry_expiration_date = (new_entry.entry_created
                                   + timedelta(expires_in))
        new_entry.save()
        return success_response
