from django.views.generic import View
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpResponse
from django.conf import settings
from drop.models import Drop
from user.models import FacebookUser
import json
import mimetypes


class DropView(View):
    def get(self, request, *args, **kwargs):
        error_response = HttpResponse(
            json.dumps({"error": "Invalid parameter."}),
                       mimetype="application/json")
        if not 'drop_id' in  request.GET and not 'querytype' in request.GET:
            return error_response
        try:
            drop = Drop.objects.get(id = request.GET['drop_id'])
        except Drop.DoesNotExist:
            return error_response

        if request.GET['querytype'] == '0':
            response = HttpResponse(
                drop.drop_content.read(),
                content_type=drop.drop_content_type
            )
        elif request.GET['querytype'] == '1':
            response = HttpResponse(json.dumps({'title': drop.drop_title, 'comment': drop.drop_comment}), mimetype="application/json")
        return response

    def post(self, request, *args, **kwargs):
        error_response = HttpResponse(
            json.dumps({"error": "Invalid parameter."}),
                       mimetype="application/json")
        if 'access_token' in request.POST and 'title' in request.POST and 'comment' in request.POST and 'type' in request.POST and 'file' in request.FILES:
            access_token = request.POST['access_token']
            user_id = FacebookUser.get_uid_by_token(access_token)
            if not user_id:
                return error_response

            user = FacebookUser.objects.filter(fbuser_id=user_id)
            if not user.exists():
                return error_response
            params = {'drop_user': user[0],
                'drop_type': request.POST['type'],
                'drop_password': '',
                'drop_content_type': 'blablabla',
                'drop_content': request.FILES['file'],
                'drop_content_type': mimetypes.guess_type(str(request.FILES['file'])),
                'drop_title': request.POST['title'],
                'drop_comment': request.POST['comment']}
            drop = Drop(**params)
            drop.save()
            return HttpResponse(json.dumps({"drop_id": drop.id}),
                                mimetype="application/json")
        else:
            return error_response
