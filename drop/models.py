from django.db import models
from user.models import FacebookUser


class Drop(models.Model):
    drop_user = models.ForeignKey(FacebookUser)
    drop_type = models.CharField(max_length=32)
    drop_password = models.CharField(max_length=32)
    drop_content = models.FileField(upload_to='files')
    drop_content_type = models.CharField(max_length=32)
    drop_title = models.CharField(max_length=32)
    drop_comment = models.CharField(max_length=128)

    def __unicode__(self):
        return self.drop_title
